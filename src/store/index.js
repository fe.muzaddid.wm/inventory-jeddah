import { createStore } from 'vuex'
import jwt_decode from "jwt-decode";

export default createStore({
  state: {
    lang: localStorage.language !== undefined ? localStorage.language : 'IN',
    isLogin: localStorage.logined !== undefined ? (localStorage.logined === 'true') : false,
    signup: localStorage.signup !== undefined ? (localStorage.signup === 'true') : false,
    keranjang: localStorage.keranjang !== '' &&
      localStorage.keranjang !== undefined ? JSON.parse(localStorage.keranjang) : [],
    userId: localStorage.token !== undefined ? jwt_decode(localStorage.token).id : null,
    userName: localStorage.token !== undefined ? jwt_decode(localStorage.token).sub : null,
    userRole: localStorage.token !== undefined ? jwt_decode(localStorage.token).role : null,
    rememberMe: localStorage.rememberme !== undefined ? (localStorage.rememberme === 'true') : false
  },
  mutations: {
    setLang (language) {
      this.state.lang = language  
    },
    setIsLogin(state) {
      state.isLogin = !state.isLogin
    },
    togleSignUp(state) {
      state.signup = !state.signup
    }
  },
  actions: {
  },
  modules: {
  }
})
