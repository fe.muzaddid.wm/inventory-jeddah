import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from './store';
import BootstrapVue3 from "bootstrap-vue-3";
import SimpleTypeahead from 'vue3-simple-typeahead';
// import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue-3/dist/bootstrap-vue-3.css";
import 'vue3-simple-typeahead/dist/vue3-simple-typeahead.css';

const app = createApp(App);
app.use(BootstrapVue3);
app.use(SimpleTypeahead);
app.use(store);
app.use(router);
app.mount("#app");
