import { createRouter, createWebHistory } from "vue-router";
import Dashboard from "@/views/DashboardView.vue"
import InventorySaudi from "@/views/InventorySaudi.vue"
import InventoryIndonesia from "@/views/InventoryIndonesia.vue"

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "dashboard.",
      icon: "ri-dashboard-line",
      role: 'ALL',
      component: Dashboard
    },
    {
      path: "/users",
      name: "users.",
      icon: "ri-file-user-line",
      role: "ADMIN",
      component: () => import("../views/UsersView.vue")
    },
    {
      path: "/product",
      name: "product.",
      icon: "ri-shopping-bag-3-line",
      role: "ADMIN",
      component: () => import("../views/ProductView.vue"),
    },
    {
      path: '/inventory',
      name: 'inventory.indonesia.saudi',
      icon: 'ri-swap-box-line',
      role: 'ADMIN',
      component: () => import("../views/Inventory.vue"),
      children: [
        {
          path: "indonesia",
          name: "indonesia",
          showName: "indonesia",
          component: InventoryIndonesia
        },
        {
          path: "saudi",
          name: "saudi",
          showName: "Saudi",
          component: InventorySaudi
        }
      ]
    },
    {
      path: '/order',
      name: 'order.products.myorder.basket',
      icon: 'ri-shopping-cart-line',
      role: 'USER',
      component: () => import("../views/Order.vue"),
      children: [
        {
          path: "products",
          name: "products",
          showName: "Products",
          component: () => import("../views/ProductView.vue")
        },
        {
          path: "myorder",
          name: "myorder",
          showName: "my order",
          component: () => import("../views/OrderView.vue")
        },
        {
          path: "basket",
          name: "basket",
          showName: "basket",
          component: () => import("../views/KeranjangView.vue")
        }
      ]
    },
    {
      path: "/request-product",
      name: "request product",
      icon: "ri-file-edit-line",
      role: "USER",
      component: () => import("../views/RequestProductView.vue")
    },
    {
      path: "/product-orders",
      name: "product orders",
      icon: "ri-shopping-cart-line",
      role: "ADMIN",
      component: () => import("../views/OrderView.vue")
    }
  ],
});

export default router;
