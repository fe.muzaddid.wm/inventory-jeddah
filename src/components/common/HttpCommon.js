import axios from 'axios';
const apiURL = import.meta.env.VITE_API_URL;
export const HTTP = axios.create({
   baseURL: apiURL,
   headers:  localStorage.token ? {
      'Authorization': 'Bearer' + ' ' + localStorage.token,
      'content-type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': 'true',
      'Access-Control-Allow-Methods': 'GET,HEAD,OPTIONS,POST,PUT',
      'Access-Control-Allow-Headers': 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers'
   } : {
      'content-type': 'application/json'
   } 
 })
 export const HTTP_FILE = axios.create({
    baseURL: apiURL,
    headers:  localStorage.token ? {
       'Authorization': 'Bearer' + ' ' + localStorage.token,
       'content-type': 'multipart/form-data',
       'Access-Control-Allow-Origin': '*',
       'Access-Control-Allow-Credentials': 'true',
       'Access-Control-Allow-Methods': 'GET,HEAD,OPTIONS,POST,PUT',
       'Access-Control-Allow-Headers': 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers'
    } : {
       'content-type': 'multipart/form-data'
    } 
  })